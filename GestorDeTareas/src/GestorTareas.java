import java.util.ArrayList;

/**
 * Clase para gestionar las tareas y sus operaciones básicas
 */
public class GestorTareas {
	private ArrayList<Tarea> listaTareas;
	private Usuario usuario;
	public GestorTareas(ArrayList<Tarea> listaTareas, Usuario usuario) {
		this.listaTareas = listaTareas;
		this.usuario = usuario;
	}

	/**
	 * Agrega una tarea al arraylist de Tareas del Gestor
	 * @param tarea
	 */
	public void agregarTarea(Tarea tarea){
		listaTareas.add(tarea);
	}

	/**
	 * Muestra las tareas del arraylist del gestor
	 */
	public void mostrarTareas(){
		for(Tarea tarea : listaTareas){
			tarea.infoBasica();
		}
	}
}
