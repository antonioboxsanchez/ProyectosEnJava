/**
 * Clase que contiene datos básicos de una tarea
 * @author Antoniobox
 * @version 1.0
 * @since 26/02/2023
 */
public class Tarea {
	private String nombre;
	private String descripcion;
	private String fecha;

	public Tarea(String nombre, String descripcion, String fecha) {
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.fecha = fecha;
	}

	/**
	 * Muestra la información básica de la tarea
	 */
	public void infoBasica(){
		System.out.println("Nombre de la tarea: " + nombre);
		System.out.println("Descripción: " + descripcion);
		System.out.println("Fecha: " + fecha);
	}
}
