/**
 * Clase de usuario, con sus datos y las tareas que tiene cada uno
 *
 * @author Antoniobox
 * @version 1.0
 * @since 26/02/2023
 */
public class Usuario {
	private String nombreUsuario;
	private String nombreCompleto;
	private String contrasena;

	public Usuario(String nombreUsuario, String nombreCompleto, String contrasena) {
		this.nombreUsuario = nombreUsuario;
		this.nombreCompleto = nombreCompleto;
		this.contrasena = contrasena;
	}

	/**
	 * Muestra la información básica del usuario
	 */
	public void infoBasica(){
		System.out.println("Nombre de usuario" + nombreUsuario);
		System.out.println("Nombre completo: " + nombreCompleto);
		System.out.println("Contraseña" + contrasena);
	}
}
